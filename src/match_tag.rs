use git2::Repository;

pub fn get_tag_match(repo: &Repository, patern: Option<&str>) -> Option<String> {
    let list_tag = match repo.tag_names(patern) {
        Ok(tag) => tag,
        Err(e) => panic!("failed to fetching tag: {}", e),
    };

    list_tag.get(0).map(|s| s.to_string())
}

fn get_sep_pos(tag: &String, sep: char) -> Option<usize> {
    let mut pos: usize = 0;
    for chr in tag.chars() {
        if chr == sep {
            return Some(pos);
        }
        pos += 1;
    }

    return None;
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_get_sep_pos_empty_str() {
        let string = String::from("");
        assert_eq!(None, get_sep_pos(&string, ' '))
    }

    #[test]
    fn test_get_sep_pos_wrong_simple_str() {
        let string = String::from("Coucou");
        assert_eq!(None, get_sep_pos(&string, ' '))
    }

    #[test]
    fn test_get_sep_pos_good_simple_str() {
        let string = String::from("Cou cou");
        assert_eq!(Some(3), get_sep_pos(&string, ' '))
    }

    #[test]
    fn test_parse_tag_empty_str() {
        let mut string = String::from("");
        let mut expex_str = String::from("");
        assert_eq!(expex_str, parse_tag(&mut string));
    }

    #[test]
    fn test_parse_tag_simple_str() {
        let mut string = String::from("my_name:my_project");
        let mut expex_str = String::from("my_project");
        assert_eq!(expex_str, parse_tag(&mut string));
    }
}
