use std::process::Command;

pub fn build_repository(path: &str, bin: &str) -> bool {
    match Command::new("sh")
        .args(&["cd", path, ";", "make CFLAGS=-DFLOPPY", bin, ";", "cd .."])
        .output()
    {
        Ok(output) => output.status.success(),
        Err(_) => false,
    }
}
