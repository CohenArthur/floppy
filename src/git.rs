use git2::{Error, Repository};
use std::fs;
use std::io;

pub fn clone_repository(url: &str, path: &str) -> Result<Repository, Error> {
    Repository::clone(url, path)
}

pub fn delete_repository(path: &str) -> Result<(), io::Error> {
    fs::remove_dir_all(path)
}

pub fn get_matching_tag(repo: &Repository, patern: Option<&str>) -> Option<String> {
    let list_tag = match repo.tag_names(patern) {
        Ok(tag) => tag,
        Err(e) => panic!("failed to fetching tag: {}", e),
    };

    list_tag.get(0).map(|s| s.to_string())
}

pub fn get_bin_name(tag: &mut String, sep: char) -> String {
    let mut pos: usize = 0;
    for chr in tag.chars() {
        if chr == sep {
            break;
        }
        pos += 1;
    }

    tag.split_off(pos)
}

#[cfg(test)]
mod tests {
    use super::*;
}
