mod build;
mod git;

use std::time::SystemTime;

const REMOTES: [&'static str; 2] = ["remote1", "remote2"];

fn main() {
    /* Get current time to fuzz only recent tags */
    let time = SystemTime::now();

    for remote in REMOTES.iter() {
        let repo = match git::clone_repository(remote, remote) {
            Ok(repo) => repo,
            Err(_) => continue,
        };

        let tag = match git::get_matching_tag(&repo, Some("")) {
            Some(tag) => tag,
            None => {
                git::delete_repository(remote);
                continue;
            }
        };

        let bin_name = git::get_bin_name(&mut tag.clone(), ':');

        if (!build::build_repo(remote, &bin_name)) {
            continue;
        }

        /* build, fork, delete */
    }
}
