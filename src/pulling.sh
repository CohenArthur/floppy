#/bin/sh

delta_time=120 # Pulling every 2 secs, can be changed

while true; do
    git pull origin master; git pull origin master --tags
    sleep "$delta_time"
done

