#!/bin/sh

PKG_LIST='config/pkg-list'
RUSTUP_INSTALLER=rustup-init.sh

#Package install
apt install < $PKG_LIST

#Rust install
curl --proto ''=https'' --tlsv1.2 -sSf https://sh.rustup.rs > "$RUSTUP_INSTALLER" && chmod +x "$RUSTUP_INSTALLER"
./$RUSTUP_INSTALLER -y
rm "$RUSTUP_INSTALLER"
source ~/.cargo/env && source ~/.rustup/env

make -f src/Makefile
