# Coding style guidelines

- C:
Coding style EPITA

- Bash:
No coding style

- Python:
PEP 8 Coding Style

- Rust:
Rust book coding style

For every file, try to have an extra newline at the end like so:

```c
int main(void)
{
    return 0;
}

```

If you're not Victor I won't bother you with it.

